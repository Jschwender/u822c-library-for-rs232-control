#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <signal.h>
#include <time.h>
#include "u822c-lib.h"
/*
  Copyright ©2023, Dipl.-Ing. (FH) J. Schwender
  Licensed unter the terms of GNU General Public License, see COPYING file.
  U822C LCR Meter. receives measurement data as soon as RMT function is enabled by the RMT key.
  In RMT function it seems sending data does not work.
  This version uses a library.
*/
char buffer[MAX_BUFFER_SIZE];
extern Setting settings;
extern Data data;
extern char *eng(double,int,int,char*);

void printall() {
    fetchData();
//    printf("%c = %.5LE  %c = %.5LE \n", settings.Fa, data.Va.val, settings.Fb, data.Vb.val);
    getRange();
    getAccuracy();
    char x1[100],x2[100],x3[100];
    eng(data.Va.val,6,0,x1);
    eng(data.Vb.val,6,0,x2);
    eng(data.Va.abstol,6,0,x3);
//    printf("%c = %.5LE  %c = %.5LE  (@%li Hz) ±%LE, Range %i Model %i\n", settings.Fa, data.Va.val, settings.Fb, data.Vb.val, settings.freq, data.Va.abstol, data.Va.range, settings.model);
    printf("%c = %s%c  %c = %s ±%.2Lf%%  ±%s%c, (@%li Hz) Range %i Model %i\n", settings.Fa, x1 ,data.Va.unit, settings.Fb, x2, data.Va.reltol*100 , x3, data.Va.unit,settings.freq, data.Va.range, settings.model);
}

int main(int argc, char* argv[]) {
    char *Device = argv[1];
    setupSerialPort(Device);      // Setup serial port
    setup_ISR();            // attach ISR
    
    lockKeyboard();

    getIdentifier();
    printf("%s\n", buffer);      // Print the received data

    setFunctionA('C');
    setFunctionB('E');
    setModel(ModSerial);

    getFunctionA();
    getFunctionB();
    getModel();
    getRange();
    printf("Settings: %c %c %li Hz  Range %i Model %i\n",settings.Fa,settings.Fb,settings.freq,data.Va.range,settings.model);

    setFrequency(100);
    printall();

    setFrequency(1000);
    printall();

    setFrequency(10000);
    printall();

    setFrequency(100000);
    printall();
//    setEModeSerial();
//    setEModeParallel();
    unlockKeyboard();
    closeSerialPort(); //close(serial_fd);    // Close the serial port

    return 0;
}	
