#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <signal.h>
#include <time.h>
#include <math.h>
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define MAX_BUFFER_SIZE 64
//#define DEBUG
/*
  ©2023, Dipl.-Ing(FH) J. Schwender
  U822C LCR Meter. receives measurement data as soon as RMT function is enabled by the RMT key.
  In RMT function it seems sending data does not work.
*/
void setupSerialPort(char *DEVICE);
int send_cmd(const char *command);    // Send data over RS232
int wait_for_ISRtrigger();
int receive_until_EOT();     // received data format is [data]<CR><LF>   or  [data]\r\n   or   [data] x13 x10
int setup_ISR();

void closeSerialPort();
void lockKeyboard();
void unlockKeyboard();
void getFunctionA();  // L C R Z NULL
void setFunctionA(char A);
void getFunctionB();  // D Q Thita Esr
void setFunctionB(char B);
void getFrequency(); // 100 120 1kHz 10kHz 100kHz
void setFrequency(); // 100 120 1000 or 1kHz…
void fetchData();    // saves a string containing "float,float,int" to the buffer
void convertData();  // picks the values from the buffer and saves float values
void getIdentifier();
void getRange();
void setRange(int range);
void getModel();
void setModel(int value);
void getAccuracy();

typedef struct { char Fa; char Fb; long int freq; int model; } Setting;
typedef struct { long double val,reltol,abstol,digit; int range; char unit;} Val;
typedef struct { Val Va,Vb,Q; } Data;

#define ModSerial 1
#define ModParallel 2

