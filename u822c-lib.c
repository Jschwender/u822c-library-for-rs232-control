#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <signal.h>
#include <time.h>
#include "u822c-lib.h"
/*
  Copyright ©2023, Dipl.-Ing. (FH) J. Schwender
  Licensed unter the terms of GNU General Public License, see COPYING file.
  EUCOL LCR Meter, Type U822C.
  Library of commands for RS232 control.
  Currently the set commands do not work as documented.
*/
int serial_fd;
// wait time after set command. It is not documented in the programming manual, but it is required.
// If you immediately continue with comands, these will fail with a beep from the device.
#define Wait_us 100000

volatile sig_atomic_t ISR_rec_trigger = 0;
extern char buffer[MAX_BUFFER_SIZE];
Setting settings;
Data data;

void signalHandler(int signum) {
    if (signum == SIGIO) {
        ISR_rec_trigger = 1;
    }
}

void setupSerialPort(char *DEVICE) {
    struct termios serial_settings;
    if ( DEVICE == NULL ) { DEVICE="/dev/ttyUSB1"; };   // the default device to use, unless specfied as argument 1 on command line
    serial_fd = open(DEVICE, O_RDWR | O_NOCTTY);    // Open the serial port
    if (serial_fd == -1) {
        perror("Error opening serial port");
        exit(EXIT_FAILURE);
    }

    // Configure serial port settings
    memset(&serial_settings, 0, sizeof(serial_settings));
    serial_settings.c_iflag = 0;
    serial_settings.c_oflag = 0;
    serial_settings.c_cflag = B9600 | CS8 | CLOCAL; // 9600 baud, 8 data bits, no parity
    serial_settings.c_lflag = 0;
    serial_settings.c_cc[VMIN] = 0;  // non-blocking read
    serial_settings.c_cc[VTIME] = 0; // inter-character timer off
    tcsetattr(serial_fd, TCSANOW, &serial_settings);

    // Set the serial port to non-blocking mode
    fcntl(serial_fd, F_SETFL, fcntl(serial_fd, F_GETFL) | O_NONBLOCK);

    // Allow the process to receive asynchronous I/O signals
    fcntl(serial_fd, F_SETOWN, getpid());
    fcntl(serial_fd, F_SETFL, fcntl(serial_fd, F_GETFL) | FASYNC);
}

#ifdef DEBUG
#define BILLION 1000000000.0
struct timespec start1, end1, start2, end2;
// for debugging detailled timing is printed to stderr. 
float calc_runtime1() {
  return((end1.tv_sec - start1.tv_sec) + (end1.tv_nsec - start1.tv_nsec) / BILLION);
}
float calc_runtime2() {
  return((end2.tv_sec - start2.tv_sec) + (end2.tv_nsec - start2.tv_nsec) / BILLION);
}
#endif



int send_cmd(const char *command) {    // Send data over RS232
#ifdef DEBUG
    clock_gettime(CLOCK_REALTIME, &start1);
#endif
    if (write(serial_fd, command, strlen(command)) < 0) {
        perror("Write error");
        close(serial_fd);
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG
    clock_gettime(CLOCK_REALTIME, &end1);
    fprintf(stderr,"  sending %s: %2.9f s (%li bytes)\n", command, calc_runtime1(), strlen(command));
#endif
}

int wait_for_ISRtrigger() {
#define SleepTime 122
    int i;
#ifdef DEBUG
    clock_gettime(CLOCK_REALTIME, &start1);
#endif
    for ( i = 0; i < 1000 && !ISR_rec_trigger; ++i) {  // ISR_rec_trigger is set by the ISR
        usleep(SleepTime); // Sleep time should be less than it takes to receive one byte at 9600bps (~1 ms)
    }                      // Sleep offloads cpu and is better than constant polling
#ifdef DEBUG
    clock_gettime(CLOCK_REALTIME, &end1);
    if ( i < 1000 ) {
	fprintf(stderr,"\r%i sleep waits (à %i µs) = %.9f until byte received\n",i,SleepTime,calc_runtime1());
    } else {
	fprintf(stderr,"\r%i sleep waits (à %i µs) = %.9f nothing received\n",i,SleepTime,calc_runtime1());
    }
#endif
}

int receive_until_EOT() {     // received data format is [data]<CR><LF>   or  [data]\r\n   or   [data] x13 x10
    int bytesReceived = 0;
    int bytesRead = 0;
#ifdef DEBUG
    clock_gettime(CLOCK_REALTIME, &start2);
#endif
    while (1) {
        wait_for_ISRtrigger();  // wait until next answer byte is received…
        ISR_rec_trigger = 0;                                      // immediately reset the ISR trigger
        bytesReceived = read(serial_fd, &buffer[bytesRead], 1);   // read the received byte into buffer
//        printf("-----%i %i %c\n", bytesReceived,bytesRead,buffer[bytesRead]);fflush(stdout);

        if (bytesReceived > 0) {
            if (bytesRead >= 2 && buffer[bytesRead - 1] == '\r' && buffer[bytesRead] == '\n') {
                buffer[bytesRead - 1 ] = 0x0;
                buffer[bytesRead] = 0x0;      // we don't want the CR+LF in the return string
                break;                        // Exit the loop when EOT is received
            }

            if (bytesRead >= MAX_BUFFER_SIZE - 1) {
                fprintf(stderr, "Received data exceeds buffer size\n");
                break;
            }
            bytesRead++;           // increment buffer index only if it is not an error or overrun
        } else if (bytesReceived < 0) {
            perror("Read error");
            close(serial_fd);
            exit(EXIT_FAILURE);
        }
    }
#ifdef DEBUG
    clock_gettime(CLOCK_REALTIME, &end2); 
    fprintf(stderr,"  wait until CR is received: %6.6f s for %i bytes\n", calc_runtime2(), bytesRead);
#endif
}

int setup_ISR() {
    struct sigaction sa;
    
    memset(&sa, 0, sizeof(sa));  // Setup signal handler for interrupt signal
    sa.sa_handler = signalHandler;
    sigaction(SIGIO, &sa, NULL);
}

void getIdentifier() {
    send_cmd("*IDN?\n");
    receive_until_EOT();
}


void closeSerialPort() {   close(serial_fd);  }

void    lockKeyboard() {   send_cmd("*LLO\n"); usleep(Wait_us); }
void  unlockKeyboard() {   send_cmd("*GTL\n"); usleep(Wait_us); }

void setUnit(char A) {
    if (A == 'L') { data.Va.unit = 'H'; }
    if (A == 'C') { data.Va.unit = 'F'; }
    if (A == 'R') { data.Va.unit = 'O'; }
}

void getFunctionA() {
    send_cmd("FUNC:impa?\n");   // L | C  | R | Z   |NULL
    receive_until_EOT();
    settings.Fa = buffer[0];
    setUnit(settings.Fa);
}
void setFunctionA(char A) {
    char sdr[20];
    sprintf(sdr,"FUNC:impa %c\n",A);
    send_cmd(sdr);   // L | C  | R | Z   |NULL
    settings.Fa = A;
    setUnit(A);
    usleep(Wait_us);   // this is not documented in the programming manual, but empirically required for set commands!!
}
void getFunctionB() {
    send_cmd("FUNC:impb?\n");   // D | Q | THITA | ESR
    receive_until_EOT();
    settings.Fb = buffer[0];   // abbreviation is ok
}
void setFunctionB(char B) {
    char sdr[20];
    if ( B == 'D') { sprintf(sdr, "FUNC:impb D\n"); }
    if ( B == 'Q') { sprintf(sdr, "FUNC:impb Q\n"); }
    if ( B == 'E') { sprintf(sdr, "FUNC:impb ESR\n"); }
    if ( B == 'T') { sprintf(sdr, "FUNC:impb THITA\n"); }
    if ( B == 'N') { sprintf(sdr, "FUNC:impb NULL\n");  }
    send_cmd(sdr);
    settings.Fb = B;
    usleep(Wait_us);   // this is not documented in the programming manual, but empirically required for set commands!!
}
void getFrequency() {
    send_cmd("FREQ?\n");   //Return 100Hz, 120Hz, 1kHz, 10kHz ,100kHz
    receive_until_EOT();
    settings.freq=atof(buffer);  // stops converting k!!
    if (strstr(buffer, "kHz") != NULL) {
	settings.freq=settings.freq*1000;
    }
}
void setFrequency(int freq) {
    char sdr[20];
    sprintf(sdr,"FREQ %i\n",freq);
    send_cmd(sdr);
    settings.freq = freq;
    usleep(100000);
}
void convertData() {
    char *ptr, *endp, *saveptr;
//    printf("string buffer %s\n",buffer);
    ptr = strtok_r(buffer,",",&saveptr);
//    printf("first part %s\n",ptr);
    data.Va.val = strtold(ptr,&endp);

    ptr = strtok_r(NULL,",",&saveptr);
//    printf("second part %s\n",ptr);
    data.Vb.val = strtold(ptr,&endp);
    
//    printf("real var %LE  %LE\n",data.Va.val,data.Vb.val);
}
void fetchData() {
    send_cmd("FETC?\n");
    receive_until_EOT();
    convertData();
}
void getRange() {
    send_cmd("FUNC:RANG?\n");   //
    receive_until_EOT();
    data.Va.range = 0;
    if (strstr(buffer, "R1") != NULL) { data.Va.range = 1; return; } // Range returns  R1 R2 R3 …
    if (strstr(buffer, "R2") != NULL) { data.Va.range = 2; return; } // Range returns  R1 R2 R3 …
    if (strstr(buffer, "R3") != NULL) { data.Va.range = 3; return; } // Range returns  R1 R2 R3 …
    if (strstr(buffer, "R4") != NULL) { data.Va.range = 4; return; } // Range returns  R1 R2 R3 …
    if (strstr(buffer, "R5") != NULL) { data.Va.range = 5; return; } // Range returns  R1 R2 R3 …
    if (strstr(buffer, "R6") != NULL) { data.Va.range = 6; return; } // Range returns  R1 R2 R3 …
    if (strstr(buffer, "R7") != NULL) { data.Va.range = 7; return; } // Range returns  R1 R2 R3 …
}
void setRange(int R) {
    char sdr[22];
    if (R > 5) { return; }
    if (R < 1) { return; }             // valid range checked
    sprintf(sdr,"FUNC:RANG R%i\n",R);
    send_cmd(sdr);
    receive_until_EOT();
    usleep(Wait_us);
}
void getModel() {
    send_cmd("FUNC:EQU?\n");   // PAR | SER
    receive_until_EOT();
    if (strstr(buffer, "PAR") != NULL) { settings.model = ModParallel; return;}
    if (strstr(buffer, "SER") != NULL) { settings.model = ModSerial;   return; }
}
void setModel(int value) {
    if (value == ModSerial )   { send_cmd("FUNC:EQU SER\n"); }
    else if (value == ModParallel ) { send_cmd("FUNC:EQU P\n"); }
    usleep(Wait_us);
}
void getAccuracy() {
    data.Va.reltol = 0.0; data.Vb.reltol = 0.0;
    long double temptol;
    if (settings.Fa == 'L') {
    	if (settings.freq < 1000) {  // for 100Hz and 120Hz
	    if (data.Va.val >= 0)      { data.Va.reltol = 0.014;   temptol = 0.00;  }  // see manual
	    if (data.Va.val >= 0.004)  { data.Va.reltol = 0.0045;  temptol = 0.0045;}
	    if (data.Va.val >= 0.04)   { data.Va.reltol = 0.001;   temptol = 0.001; }
	    if (data.Va.val >= 40)     { data.Va.reltol = 0.0035;  temptol = 0.0035;}
	    if (data.Va.val >= 400)    { data.Va.reltol = 0.01;    temptol = 0.01;  }
	}
    	if (settings.freq == 1000) {
	    if (data.Va.val >= 0)      { data.Va.reltol = 0.014;   temptol = 0.00;  }  // see manual
	    if (data.Va.val >= 400e-6) { data.Va.reltol = 0.0045;  temptol = 0.0045;}
	    if (data.Va.val >= 0.004)  { data.Va.reltol = 0.001;   temptol = 0.001; }
	    if (data.Va.val >= 4)      { data.Va.reltol = 0.0035;  temptol = 0.0035;}
	    if (data.Va.val >= 40)     { data.Va.reltol = 0.01;    temptol = 0.01;  }
	}
    	if (settings.freq == 10000) {
	    if (data.Va.val >= 0)      { data.Va.reltol = 0.014;   temptol = 0.00;   }  // see manual
	    if (data.Va.val >= 40e-6)  { data.Va.reltol = 0.0045;  temptol = 0.0045; }
	    if (data.Va.val >= 400e-6) { data.Va.reltol = 0.003;   temptol = 0.003;  }
	    if (data.Va.val >= 0.004)  { data.Va.reltol = 0.001;   temptol = 0.001;  }
	    if (data.Va.val >= 0.04)   { data.Va.reltol = 0.0035;  temptol = 0.0035; }
	    if (data.Va.val >= 0.4)    { data.Va.reltol = 0.008;   temptol = 0.008;  }
	}
    	if (settings.freq == 100000) {
	    if (data.Va.val >= 0)      { data.Va.reltol = 0.025;  temptol = 0.00;  }  // see manual
	    if (data.Va.val >= 4e-6)   { data.Va.reltol = 0.008;  temptol = 0.008; }
	    if (data.Va.val >= 40e-6)  { data.Va.reltol = 0.005;  temptol = 0.005; }
	    if (data.Va.val >= 400e-6) { data.Va.reltol = 0.005;  temptol = 0.005; }
	    if (data.Va.val >= 0.004)  { data.Va.reltol = 0.008;  temptol = 0.008; }
	    if (data.Va.val >= 0.04)   { data.Va.reltol = 0.012;  temptol = 0.012; }
	}
	if ( settings.Fb == 'D' ) { data.Vb.reltol = temptol;}
	if ( settings.Fb == 'Q' ) { data.Vb.reltol = (pow(data.Vb.val,2)*temptol)/(1+data.Vb.val*temptol); }
	if ( settings.Fb == 'E' ) { data.Vb.reltol = 2.0*M_PI*settings.freq*data.Va.val * M_PI*temptol/180.0;  }
	data.Vb.abstol = temptol * data.Vb.val; 
    }
    if (settings.Fa == 'C') {
    	if (settings.freq < 1000) {  // for 100Hz and 120Hz
	    if (data.Va.val >= 0)      { data.Va.reltol = 0.0125;  temptol = 0.00;  }  // see manual
	    if (data.Va.val >= 4e-9)   { data.Va.reltol = 0.0035;  temptol = 0.0035;}
	    if (data.Va.val >= 40e-9)  { data.Va.reltol = 0.001;   temptol = 0.001; }
	    if (data.Va.val >= 40e-6)  { data.Va.reltol = 0.0035;  temptol = 0.0035;}
	    if (data.Va.val >= 400e-6) { data.Va.reltol = 0.01;    temptol = 0.01;  }
	    if (data.Va.val >= 4e-3)   { data.Va.reltol = 0.05;    temptol = 0.05;  }
	}
    	if (settings.freq == 1000) {
	    if (data.Va.val >= 0)      { data.Va.reltol = 0.0125;  temptol = 0.00;  }  // see manual
	    if (data.Va.val >= 400e-12){ data.Va.reltol = 0.0035;  temptol = 0.0035;}
	    if (data.Va.val >= 400e-12){ data.Va.reltol = 0.001;   temptol = 0.001; }
	    if (data.Va.val >= 4e-6)   { data.Va.reltol = 0.0035;  temptol = 0.0035;}
	    if (data.Va.val >= 40e-6)  { data.Va.reltol = 0.01;    temptol = 0.01;  }
	    if (data.Va.val >= 400e-6) { data.Va.reltol = 0.02;    temptol = 0.02;  }
	}
    	if (settings.freq == 10000) {
	    if (data.Va.val >= 0)      { data.Va.reltol = 0.0125;  temptol = 0.00;   }  // see manual
	    if (data.Va.val >= 40e-12) { data.Va.reltol = 0.0035;  temptol = 0.0045; }
	    if (data.Va.val >= 400e-12){ data.Va.reltol = 0.001;   temptol = 0.003;  }
	    if (data.Va.val >= 400e-9) { data.Va.reltol = 0.0035;  temptol = 0.0035; }
	    if (data.Va.val >= 4e-6)   { data.Va.reltol = 0.015;   temptol = 0.015;  }
	    if (data.Va.val >= 40e-6)  { data.Va.reltol = 0.03;    temptol = 0.03;   }
	}
    	if (settings.freq == 100000) {
	    if (data.Va.val >= 0)      { data.Va.reltol = 0.03;   temptol = 0.00;}  // see manual
	    if (data.Va.val >= 4e-12)  { data.Va.reltol = 0.012;  temptol = 0.012; }
	    if (data.Va.val >= 40e-12) { data.Va.reltol = 0.008;  temptol = 0.008;}
	    if (data.Va.val >= 4e-9)   { data.Va.reltol = 0.005;  temptol = 0.005;}
	    if (data.Va.val >= 40e-9)  { data.Va.reltol = 0.008;  temptol = 0.008;}
	    if (data.Va.val >= 400e-9) { data.Va.reltol = 0.025;  temptol = 0.025;}
	    if (data.Va.val >= 4e-6)   { data.Va.reltol = 0.06;   temptol = 0.06;}
	}
	if ( settings.Fb == 'D' || settings.Fb == 'E' ) { 
	        data.Vb.reltol = temptol; 
	        data.Vb.abstol = temptol * data.Vb.val; }
	if ( settings.Fb == 'Q' ) {
	        data.Vb.reltol = (pow(data.Vb.val,2)*data.Va.reltol)/(1+data.Vb.val*data.Va.reltol); }
    }
    if (settings.Fa == 'Z') {
        const long double rad = M_PI/180.0;
    	if (settings.freq == 10000) {
	    if (data.Va.val >= 0)      { data.Va.reltol = 0.03;   temptol = 0.00;}  // see manual
	    if (data.Va.val >= 0.4)    { data.Va.reltol = 0.01;   temptol = 0.6; }
	    if (data.Va.val >= 4)      { data.Va.reltol = 0.0035; temptol = 0.25;}
	    if (data.Va.val >= 40)     { data.Va.reltol = 0.001;  temptol = 0.1;}
	    if (data.Va.val >= 40e3)   { data.Va.reltol = 0.0035; temptol = 0.25;}
	    if (data.Va.val >= 400e3)  { data.Va.reltol = 0.0125; temptol = 0.75;}
	    if (data.Va.val >= 4e6)    { data.Va.reltol = 0.03;   temptol = 1.75;}
	}
    	if (settings.freq == 100000) {
	    if (data.Va.val >= 0)      { data.Va.reltol = 0.06;    temptol = 0.00; }  // see manual
	    if (data.Va.val >= 0.4)    { data.Va.reltol = 0.025;   temptol = 1.43; }
	    if (data.Va.val >= 4)      { data.Va.reltol = 0.008;   temptol = 0.46; }
	    if (data.Va.val >= 40)     { data.Va.reltol = 0.005;   temptol = 0.3;  }
	    if (data.Va.val >= 4000)   { data.Va.reltol = 0.008;   temptol = 0.46; }
	    if (data.Va.val >= 40e3)   { data.Va.reltol = 0.012;   temptol = 0.69; }
	    if (data.Va.val >= 400e3)  { data.Va.reltol = 0.03;    temptol = 1.75; }
	    if (data.Va.val >= 4e6)    { data.Va.reltol = 0.03;    temptol = 4.6;  }
	}
	if ( settings.Fb == 'T' ) { data.Vb.reltol = temptol; }
	if ( settings.Fb == 'E') {  // ESR as second value
	    if ( settings.model == ModSerial )   { data.Vb.reltol = data.Vb.val * temptol; }
	    if ( settings.model == ModParallel ) { data.Vb.reltol = data.Vb.val * temptol; }
	}
	data.Vb.abstol = temptol * data.Vb.val;
    }
    data.Va.abstol = data.Va.val * data.Va.reltol;
}