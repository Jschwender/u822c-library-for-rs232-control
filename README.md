# U822C library for RS232 control



## Getting started
<img src="u822.jpg">
RS232 communication with EUCOL measurement device for LCR measurement.
According to the programming guide it is possible to set settings and read measurement results.
This is an implementation in C.

## Examples how to use

there are example code that shows how to utilize the library.

This is how the output looks like on the console:
<pre>
$ ./u822c-example1
U822C Handheld LCR Meter,VER3.3.1321,SN103H201012
Settings: C E 0 Hz  Range 3 Model 1
C =  981.056 nF  E =  46.3463 m ±0.10%  ± 981.056 pF, (@100 Hz) Range 2 Model 1
C =  981.186 nF  E =  46.3463 m ±0.10%  ± 981.186 pF, (@1000 Hz) Range 3 Model 1
C =  981.377 nF  E =  46.3463 m ±0.35%  ± 3.43482 nF, (@10000 Hz) Range 3 Model 1
C =  1.06568 µF  E =  46.3463 m ±2.50%  ± 26.6420 nF, (@100000 Hz) Range 4 Model 1


$ ./u822c-receiver
 1.06543 µ ± 1.06543 n   46.3463 m ±0.0
 1.06545 µ ± 1.06545 n   46.3463 m ±0.0
 1.06550 µ ± 1.06550 n   46.3463 m ±0.0
 1.06550 µ ± 1.06550 n   46.3463 m ±0.0
^C
</pre>
## About the Device
The device is a fairly accurate instrument for the price. It comes with Kelvin connectors
which is very good for low impedance measurements. As with such devices, they give you always 
numbers on the display, but they don't tell you what accuracy this number is. 
Accuracy actually depends not only on temperature but also on the range. So the nominal accuracy 
for this device is ±0.1%. But if you measure a capacity of 4700 µF at 100 kHz the accuracy is 
much larger at ±6%+20 digits. This is properly specified in the manual,
but you probably don't have that large table in mind when you read the display.
Naturally people tend to take these measurement numbers from digital display literally,
which generally is a mistake.

This software gives you more than the display, it also displays the accuracy with each measurement.
Beyond that it enables you to automate series of measurements over frequency, for example.