/* Print a floating-point number in engineering notation 
   derived from: http://www.cs.tut.fi/~jkorpela/c/eng.html 
   modification: the finction get a pointer to the string, 
   which is overwritte with thte result.
 */

#include <stdio.h>
#include <math.h>

#define PREFIX_START (-24)
#define PREFIX_END 24

void eng(double value, int digits, int numeric, char *result)
{
      int expof10;
      char vz;
//               starting from 10·E-24       to      10·E24
  static char *prefix[] = { "y", "z", "a", "f", "p", "n", "µ", "m", "",  "k", "M", "G", "T", "P", "E", "Z", "Y"  }; 

      if (value < 0.0)
        {
            vz = '-';
            value = -value;
        } else { vz = ' '; };
      if (value == 0.0)
        {
            sprintf(result,"0.0");
	    return;
        }

      expof10 = (int) log10(value);
      if(expof10 > 0)
        expof10 = (expof10/3)*3;
      else
        expof10 = (-expof10+3)/3*(-3); 
 
      value *= pow(10,-expof10);

      if (value >= 1000.)
         { value /= 1000.0; expof10 += 3; }
      else if(value >= 100.0)
         digits -= 2;
      else if(value >= 10.0)
         digits -= 1;

      if(numeric || (expof10 < PREFIX_START) ||    
                    (expof10 > PREFIX_END))
        sprintf(result, "%c%.*fe%d", vz, digits-1, value, expof10); 
      else
        sprintf(result, "%c%.*f %s", vz, digits-1, value, prefix[(expof10-PREFIX_START)/3]);

}
