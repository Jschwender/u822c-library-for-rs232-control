#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <signal.h>
#include <time.h>
#include "u822c-lib.h"
/*
  Copyright ©2023, Dipl.-Ing. (FH) J. Schwender
  Licensed unter the terms of GNU General Public License, see COPYING file.
  In RMT function it seems sending data does not work.
  This version uses a library.
*/
char buffer[MAX_BUFFER_SIZE];
extern char *eng(double,int,int,char*);

//extern Accuracy tol;
extern Setting settings;
extern Data data;
char x1[100],x2[100],x3[100],x4[100];

int main(int argc, char* argv[]) {
    char *Device = argv[1];
    setupSerialPort(Device);      // Setup serial port
    setup_ISR();            // attach ISR
    getFunctionA();
    getFunctionB();
    while (1) {
	receive_until_EOT();
	convertData();
	getAccuracy();
	eng(data.Va.val,6,0,x1);
	eng(data.Vb.val,6,0,x2);
	eng(data.Va.abstol,6,0,x3);
	eng(data.Vb.abstol,6,0,x4);
//	printf("%.9LE %.9LE\n",data.Va.val,data.Vb.val);
//	printf("%s ±%.3Lf%%  %s ±%.3Lf%%\n",x1, data.Va.reltol*100, x2, data.Vb.reltol*100);
	printf("%s ±%s  %s ±%s\n",x1, x3, x2, x4);
//	printf("%s", buffer);      // Print the received data
//	printf(" %.2Lf%%\n", data.Va.reltol*100);      // Print the received data
    }
    closeSerialPort(); //close(serial_fd);    // Close the serial port

    return 0;
}	
